#include <iostream>
#include <string>
#include <cstring>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif

class UDPSocket
{
public:
    UDPSocket() : m_socket(0), m_isOpen(false) {}
    ~UDPSocket()
    {
        close();
    }

    bool open(unsigned short port)
    {
        if (m_isOpen)
        {
            std::cerr << "Socket is already open" << std::endl;
            return false;
        }

#ifdef _WIN32
        // Initialize Winsock
        WSADATA wsaData;
        int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (result != 0)
        {
            std::cerr << "WSAStartup failed: " << result << std::endl;
            return false;
        }
#endif

        m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (m_socket == -1)
        {
            std::cerr << "Socket creation failed" << std::endl;
            return false;
        }

        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = htonl(INADDR_ANY);

        if (bind(m_socket, (struct sockaddr*)&addr, sizeof(addr)) == -1)
        {
            std::cerr << "Bind failed" << std::endl;
            close();
            return false;
        }

        m_isOpen = true;
        return true;
    }

    void close()
    {
        if (m_socket != -1)
        {
#ifdef _WIN32
            closesocket(m_socket);
            WSACleanup();
#else
            ::close(m_socket);
#endif
            m_socket = -1;
            m_isOpen = false;
        }
    }

    bool isOpen() const
    {
        return m_isOpen;
    }

    bool send(const std::string& message, const std::string& address, int port)
    {
        if (!m_isOpen)
        {
            std::cerr << "Socket is not open" << std::endl;
            return false;
        }

        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        inet_pton(AF_INET, address.c_str(), &addr.sin_addr);

        int result = sendto(m_socket, message.c_str(), message.length(), 0, (struct sockaddr*)&addr, sizeof(addr));
        if (result == -1)
        {
            std::cerr << "Send failed: " << errno << std::endl;
            return false;
        }

        return true;
    }

    bool receive(std::string& message, std::string& address, int& port)
    {
        if (!m_isOpen)
        {
            std::cerr << "Socket is not open" << std::endl;
            return false;
        }

        char buffer[1024];
        memset(&m_sockAddr, 0, sizeof(m_sockAddr));
        socklen_t fromLength = sizeof(m_sockAddr);

        int result = recvfrom(m_socket, buffer, sizeof(buffer), 0, (struct sockaddr*)&m
if (result == SOCKET_ERROR)
{
    std::cerr << "Receive failed: " << WSAGetLastError() << std::endl;
    return false;
}

message = std::string(buffer, result);
address = inet_ntoa(m_sockAddr.sin_addr);
port = ntohs(m_sockAddr.sin_port);

return true;
}



int main()
{
    const unsigned short PORT = 1234;
    const std::string ADDRESS = "127.0.0.1";

    UDPSocket socket;
    if (!socket.open(PORT))
    {
        std::cerr << "Failed to open socket" << std::endl;
        return 1;
    }

    std::string message = "Hello, world!";
    if (!socket.send(message, ADDRESS, PORT))
    {
        std::cerr << "Failed to send message" << std::endl;
        return 1;
    }

    std::string receivedMessage;
    std::string senderAddress;
    int senderPort;
    if (!socket.receive(receivedMessage, senderAddress, senderPort))
    {
        std::cerr << "Failed to receive message" << std::endl;
        return 1;
    }

    std::cout << "Received message: " << receivedMessage << " from " << senderAddress << ":" << senderPort << std::endl;

    return 0;
}
