#include <iostream>
#include <boost/asio.hpp>

using boost::asio::ip::udp;

class FGNetClient {
public:
    FGNetClient(const std::string& address, int port) : endpoint_(boost::asio::ip::address::from_string(address), port) {
        socket_ = std::make_unique<udp::socket>(io_service_, udp::v4());
    }

    void sendCommand(const std::string& cmd) {
        socket_->send_to(boost::asio::buffer(cmd), endpoint_);
    }

    std::string receiveResponse() {
        char response[1024];
        udp::endpoint sender_endpoint;
        size_t response_length = socket_->receive_from(boost::asio::buffer(response), sender_endpoint);
        return std::string(response, response_length);
    }

private:
    boost::asio::io_service io_service_;
    std::unique_ptr<udp::socket> socket_;
    udp::endpoint endpoint_;
};

int main()
{
    try {
        FGNetClient client("127.0.0.1", 5501);

        // Send command to set altitude
        std::string cmd = "set /autopilot/settings/altitude-ft 10000\n";
        client.sendCommand(cmd);

        // Receive response from FlightGear
        std::string response = client.receiveResponse();
        std::cout << "Response from FlightGear: " << response << std::endl;

    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
